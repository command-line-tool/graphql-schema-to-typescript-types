// TODO: union and | are not managed yet

// Convert graphql comments '#' to typescript '//'
export const comments = (s: string) => s.replace(/^([^#]*)#(.*)$/mg, "$1//$2");


// suppress schema block
export const schema = (s: string) => s.replace(/\bschema\b\s*{[^}]*}/mg, '');

// suppress Query, Mutation and Subscription blocks
export const resolvers = (s: string) => s.replace(/\btype\b\s+\b(Query|Mutation|Subscription)\b\s*{[^}]*}/mg, '');

// Convert native types to typescript by using lowercase
export const lowercaseTypes = (s: string) => s.replace(/\b(String|Number|Boolean)\b/mg, (t) => t.toLowerCase());

// Translate graphql native types to Typescript using translation map
const _convertMap : {[k:string]:string} = {'ID': 'string', 'Float': 'number', 'Int': 'number'};
export const convertType = (s: string) => s.replace(/\bID|Float|Int\b/mg, (t) => _convertMap[t]);

// convert graphql `type {}` syntax to typescript `type = {}` syntax
const _types = /(.*(?=\btype\b|}))(?:}|\btype\b\s+\b([A-Za-z][A-Za-z0-9_]*)\b\s*{([^}]*)})/mg;

// convert graphql `attribut: type` to typescript `attribut: type` syntax
// name : type!   => name  : type
// name : type    => name? : type
// name : [type]! => name  : type[]
// name : [type]  => name? : type[]
const _attributes = /([^A-Za-z]*)(?:}|\b([A-Za-z][A-Za-z0-9_]*)\b\s*:\s*(?:{|(\[)?\s*\b([A-Za-z][A-Za-z0-9_]*)\b\s*(!)?\s*(\])?\s*(!)?))/mg;


export const complexTypes = (s: string) => {
  let type;
  let nestedComplexAttribute = 0;
  let buf = '';
  while((type = _types.exec(s)) !== null) {
    // console.log(':::::::::::::::::::::::::::::::::::::::;\n', type[0], ':::::::::::::::::::::::::::::::::::::::;\n')

    // buf += type[1]
    if (type[2] === undefined) {
      nestedComplexAttribute -= 1;
      buf += `  }\n`;
      continue;
    }
    buf += `export type ${type[2]} = {`;
    let attribute;
    while((attribute = _attributes.exec(type[3])) !== null) {
      // console.log('*******************\n', attribute[0], '*********************************\n')
      buf += attribute[1]
      // 1 before
      // 2 attribute name
      // 3 [
      // 4 type
      // 5 !
      // 6 ]
      // 7 !

      
      if (attribute[2] === undefined) { // close } for complex attribute
        nestedComplexAttribute -= 1;
        buf += `  }`;
      } else if (attribute[4] === undefined) { // Complex attribute (e.g. Personne: { ... : ... })
        nestedComplexAttribute += 1;
        buf += `  ${attribute[2]} : {`;
      } else if (attribute[3] === '[' && attribute[6] === ']') { // array
        buf += `  ${attribute[2]}${attribute[7]?"":"?"} : ${attribute[4]}[]`;
      } else if (!attribute[3] && !attribute[6]) { // not an array
        buf += `  ${attribute[2]}${attribute[5]?"":"?"} : ${attribute[4]}`;
      } else {
        console.log('ERROR: square braket mistmatch:', attribute[0]);
        console.log('ERROR:', attribute);
        process.exit(-1);
      }
      buf += '\n';
    }
    buf += '}\n';
  }
  return buf;  
}
