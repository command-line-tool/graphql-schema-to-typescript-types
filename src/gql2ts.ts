import fs from 'fs';
import {comments, schema, resolvers, lowercaseTypes, convertType, complexTypes} from './lib/graphql';

export const main = (args: string[]) => {
  const typescript = args.map(filename => {
    if (!fs.existsSync(filename)) {
      console.log((new Date().toISOString()), `gql2ts: file "${filename}" not found.`);
      process.exit(1);
    }
    // console.log((new Date().toISOString()), 'gql2ts: args=', JSON.stringify(args));
    const ts = [fs.readFileSync(filename, 'utf8')]
      .map(comments)
      .map(schema)
      .map(resolvers)
      .map(lowercaseTypes)
      .map(convertType)
      .map(complexTypes)
      .reduce( (acc, value) => value, "");
    console.log(ts);
  })
  .join("\n");
  console.log(typescript);
};

// main(process.argv.splice(2));